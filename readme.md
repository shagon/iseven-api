# isEven API

### Tells if a number is even

Features:
- Plug and play replacement for https://isevenapi.xyz/
- Fully backwards compatible API with iseven API
- Tests included
- MUCH larger range (-170141183460469231731687303715884105728 to 170141183460469231731687303715884105727)
- Supports negative numbers
- No subscription model - fully open source
- Written in rust
- Function documentation


## Starting

The binaries are reproducible, build them with `cargo build --release`

## Libraries used

```toml
tokio = { version = "1", features = ["full"] }
warp = "0.3"
```

