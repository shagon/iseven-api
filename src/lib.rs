/// Returns a bool if number is even
///
/// # Arguments
///
/// * `number` - i128 number
///
/// # Examples
///
/// ```
/// use iseven::is_even;
/// is_even(6); // returns true
/// is_even(7); // returns false
/// ```
pub fn is_even(number: i128) -> bool {
    (number % 2) == 0
}

#[cfg(test)]
mod tests {
    use std::i128;

    use super::*;
    // Tests if a number is even
    #[test]
    fn number_is_even() {
        assert_eq!(is_even(6), true);
    }

    // Tests if a number is not even
    #[test]
    fn number_is_not_even() {
        assert_eq!(is_even(i128::MAX), false);
    }

    // Tests if a negative number is even
    #[test]
    fn negative_number_is_even() {
        assert_eq!(is_even(i128::MIN), true);
    }

    // Tests if a negative number is not even
    #[test]
    fn negative_number_is_not_even() {
        assert_eq!(is_even(-5), false);
    }
}
