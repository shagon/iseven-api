use iseven::is_even;
use std::collections::HashMap;
use warp::Filter;

#[tokio::main]
async fn main() {
    // GET /api/iseven/6 => 200 OK with body {"iseven": true }
    let iseven = warp::path!("api" / "iseven" / i128).map(|number: i128| {
        let mut res: HashMap<&str, bool> = HashMap::new();
        res.insert("iseven", is_even(number));
        warp::reply::json(&res)
    });

    warp::serve(iseven).run(([127, 0, 0, 1], 3030)).await;
}
